﻿Imports System.IO
Imports System.Security.Cryptography
Imports System.Text

Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If (Not String.IsNullOrEmpty(TextBox1.Text)) Then
            TextBox3.Text = Me.Decrypt(Me.TextBox1.Text, Me.TextBox2.Text)
            If (Not String.IsNullOrEmpty(TextBox3.Text)) Then
                TextBox3.SelectionStart = 0
                TextBox3.SelectionLength = TextBox3.Text.Length
            End If
            TextBox3.Copy()
            MsgBox("Done Decrypted & Copy : " + TextBox3.Text, MsgBoxStyle.Information)
        Else
            MsgBox("Add String to Decrypt", MsgBoxStyle.Critical)
        End If

    End Sub


    Public Function Decrypt(encrypted As String, secretKey As String) As String
        Dim result As String = Nothing
        Using memoryStream As MemoryStream = New MemoryStream(Convert.FromBase64String(encrypted))
            Dim algorithm As RijndaelManaged = Me.getAlgorithm(secretKey)
            Using cryptoStream As CryptoStream = New CryptoStream(memoryStream, algorithm.CreateDecryptor(), CryptoStreamMode.Read)
                Dim array As Byte() = New Byte(CInt((memoryStream.Length - 1L)) + 1 - 1) {}
                Dim count As Integer = cryptoStream.Read(array, 0, CInt(memoryStream.Length))
                result = Encoding.Unicode.GetString(array, 0, count)
            End Using
        End Using
        Return result
    End Function

    Private Function getAlgorithm(secretKey As String) As RijndaelManaged
        Dim rfc2898DeriveBytes As Rfc2898DeriveBytes = New Rfc2898DeriveBytes(secretKey, Encoding.Unicode.GetBytes("099u787978786"))
        Dim rijndaelManaged As RijndaelManaged = New RijndaelManaged()
        rijndaelManaged.KeySize = 256
        rijndaelManaged.IV = rfc2898DeriveBytes.GetBytes(CInt(Math.Round(CDbl(rijndaelManaged.BlockSize) / 8.0)))
        rijndaelManaged.Key = rfc2898DeriveBytes.GetBytes(CInt(Math.Round(CDbl(rijndaelManaged.KeySize) / 8.0)))
        rijndaelManaged.Padding = PaddingMode.PKCS7
        Return rijndaelManaged
    End Function

End Class
